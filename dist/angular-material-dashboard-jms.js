/*
 * Copyright (c) 2015-2025 Phoinex Scholars Co. http://dpq.co.ir
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

angular.module('ngMaterialDashboardJms', [//
	'ngMaterialDashboard',//
	'seen-supertenant'
]);

/*
 * Copyright (c) 2015-2025 Phoinex Scholars Co. http://dpq.co.ir
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

angular.module('ngMaterialDashboardJms')
/**
 * 
 */
.config(function ($routeProvider) {
    $routeProvider
    .otherwise('/pipelines')
    /**
     * @ngdoc ngRoute
     * @name AmdJmsPipelinesCtrl
     * @description List of pipelines
     * 
     */
    .when('/pipelines', {
        controller: 'AmdJmsPipelinesCtrl',
        templateUrl: 'views/amd-jms-pipelines.html',
        navigate: true,
        groups: ['jms'],
        name: 'Jobs',
        icon: 'dvr',
        /*
         * @ngInject
         */
        protect: function ($rootScope) {
            return !$rootScope.__account.permissions.tenant_owner && !$rootScope.__account.permissions.tenant_member;
        }
    });
});
'use strict';
angular.module('ngMaterialDashboardJms')

/**
 * @ngdoc controller
 * @name AmdJmsPipelineCtrl
 * @description pipeline controller
 * 
 * Manages a pipeline view
 */
.controller('AmdJmsPipelineCtrl', function ($scope, $jms, $routeParams, $navigator) {

	/**
     * Remove pipeline
     * 
     * @memberof AmdJmsPipelineCtrl
     * @return {promiss} to remove the pipeline
     */
	function remove() {
	    if($scope.loadingPipeline) {
	        return;
	    }
		confirm('The pipeline will be deleted. There is no undo action.')//
		.then(function(){
			return $scope.loadingPipeline = $scope.pipeline.delete()//
			.then(function(){
                $navigator.openPage('pipelines');
            }, function(){
                alert('Fail to delete pipeline.');
            });
		})//
		.finally(function(){
		    $scope.loadingPipeline = false;
		});
	}
	
	/**
     * Save changes
     * 
     * Save all changed of the current pipeline
     * 
     * @memberof AmdJmsPipelineCtrl
     * @return {promiss} to save the pipeline
     */
	function save(){
		if($scope.loadingPipeline){
			return;
		}
		return $scope.loadingPipeline = $scope.pipeline.update()//
		.then(function(){
			toast('Pipeline is saved');
		})//
		.finally(function(){
		    $scope.loadingPipeline = false;
		});
	}

	/**
     * Load the pipeline
     * 
     * @memberof AmdJmsPipelineCtrl
     * @return {promiss} to load the pipeline
     */
	function load() {
	    if($scope.loadingPipeline){
	        return;
	    }
	    $scope.loadingPipeline = true;
		return $jms.getPipeline($routeParams.id)//
		.then(function(pipeline){
			$scope.pipeline = pipeline;
		})
		.finally(function(){
		    $scope.loadingPipeline = false;
		});
	}

	$scope.remove = remove;
	$scope.save = save;
	load();
});
/* 
 * The MIT License (MIT)
 * 
 * Copyright (c) 2016 weburger
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

angular.module('ngMaterialDashboardJms')

/**
 * @ngdoc controller
 * @name AmdJmsPipelinesCtrl
 * @description # AmdJmsPipelinesCtrl Controller of the pipelines
 */
.controller('AmdJmsPipelinesCtrl',
function($scope, $superjms, $supertenant, $translate, $monitor, $q, 
		QueryParameter) {

	var paginatorParameter = new QueryParameter();
// paginatorParameter.addFilter('status', 'in-progress');
	var requests = null;
	var ctrl = {
		items: ['hadi']
	};

	function loadTenants(res){
		// Fetch IDs of tenants
		var idSet = new Set();
		res.items.forEach(function(value){
			idSet.add(value.tenant);			
		});
		var qp = new QueryParameter();
		idSet.forEach(function(id){
			qp.addFilter('id', id);
		});
		return $supertenant.getTenants(qp)//
		.then(function(tList){
			if(!$scope.tenants){
				$scope.tenants = [];
			}
			tList.items.forEach(function(t){
				$scope.tenants[t.id] = t;
			});
		});
	}
	
	/**
	 * لود کردن داده‌های صفحه بعد
	 * 
	 * @returns
	 */
	function nextPage() {
		if (ctrl.loadingPipelines || (requests && !requests.hasMore())) {
			return;
		}
		if (requests) {
			paginatorParameter.setPage(requests.getNextPageIndex());
		}
		ctrl.loadingPipelines = true;
		$superjms.getPipelines(paginatorParameter)//
		.then(function(items) {
			requests = items;
			ctrl.items = ctrl.items.concat(requests.items);
			return items;
		}, function() {
			alert($translate.instant('Failed to load pipelines.'));
		})//
		.then(loadTenants, function(){
			throw 'Failed to load tenants of pipelines';
		})
		.finally(function(){
			ctrl.loadingPipelines = false;
		});
	}

	/**
	 * تمام حالت‌های کنترل ررا بدوباره مقدار دهی می‌کند.
	 * 
	 * @returns
	 */
	function reload() {
		requests = null;
		ctrl.items = [];
		nextPage();
	}

	function loadStatistics(){
		ctrl.loadingStatistics = true;
		$scope.metrics = {
			'supertms.test.count': 0,
			'supertms.test_run.count': 0,
			'supertenant.tenant.count': 0,
			'supertenant.member.count': 0
		};
		var promiseList = [];
		for(var metric in $scope.metrics){
			var promise = $monitor.getMetric(metric)//
			.then(function(res){
				$scope.metrics[res.name] = res.value;
			});
			promiseList.push(promise);
		}
		$q.all(promiseList)//
		.finally(function(){
			$scope.ctrl.loadingStatistics = false;
		});
	}
	
	$scope.ctrl = ctrl;
	$scope.nextPage = nextPage;
	$scope.paginatorParameter = paginatorParameter;
	$scope.reload = reload;
	
	loadStatistics();
});


/*
 * Copyright (c) 2015-2025 Phoinex Scholars Co. http://dpq.co.ir
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

angular.module('ngMaterialDashboardJms')
/*
 * Addig job management
 */
.run(function($navigator) {
    $navigator.newGroup({
        id: 'jms',
        title: 'Job management',
        icon: 'search',
        priority : 4,
        hidden: '!app.user.tenant_owner'
    });
});
/*
 * Copyright (c) 2015-2025 Phoinex Scholars Co. http://dpq.co.ir
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

angular.module('ngMaterialDashboardJms')

/**
 * @ngdoc Factories
 * @name JmsPipeline
 * @description a pipeline manager
 * 
 */
.factory('JmsPipeline', seen.factory({
    url : '/api/v2/jms/pipelines'
})) //

/**
 * @ngdoc service
 * @name $jms
 * @description Job management service
 * 
 * Manages jobs
 * 
 */
.service('$jms', seen.service({
    resources : [ {
        name : 'Pipeline',
        factory : 'JmsPipeline',
        type : 'collection',
        url : '/api/v2/jms/pipelines'
    } ]
}));

/*
 * Copyright (c) 2015-2025 Phoinex Scholars Co. http://dpq.co.ir
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

angular.module('ngMaterialDashboardJms')

/**
 * @ngdoc Factories
 * @name SuperJmsPipeline
 * @description the pipline access
 * 
 */
.factory('SuperJmsPipeline', seen.factory({
    url : '/api/v2/superjms/pipelines'
})) //

/**
 * @ngdoc service
 * @name $superjms
 * @description Job management service on all tenants
 * 
 * Manages jobs of all tenants
 * 
 */
.service('$superjms', seen.service({
    resources : [ {
        name : 'Pipeline',
        factory : 'SuperJmsPipeline',
        type : 'collection',
        url : '/api/v2/superjms/pipelines'
    } ]
}));

angular.module('ngMaterialDashboardJms').run(['$templateCache', function($templateCache) {
  'use strict';

  $templateCache.put('views/amd-jms-pipeline-new.html',
    ""
  );


  $templateCache.put('views/amd-jms-pipeline.html',
    ""
  );


  $templateCache.put('views/amd-jms-pipelines.html',
    "<md-content flex layout-padding mb-infinate-scroll=nextPage()> <mb-titled-block mb-icon=report mb-title=\"{{'Statistics' | translate}}\" mb-progress=ctrl.loadingStatistics> <div layout=row layout-align=\"center center\" layout-wrap layout-padding> <md-card style=\"width: 40%\" class=value-card> <md-card-title> <md-card-title-text style=\"margin-left: 16px\"> <span class=md-headline>{{metrics['supertenant.tenant.count'] || 0}}</span> <span class=md-subhead translate>Number of Organizations</span>  </md-card-title-text> <md-card-title-media> <div class=\"md-media-sm card-media\"> <wb-icon size=80px>business</wb-icon> </div> </md-card-title-media> </md-card-title> </md-card> <md-card style=\"width: 40%\" class=value-card> <md-card-title> <md-card-title-text style=\"margin-left: 16px\"> <span class=md-headline>{{metrics['supertenant.member.count'] || 0}}</span> <span class=md-subhead translate>Number of Users</span>  </md-card-title-text> <md-card-title-media> <div class=\"md-media-sm card-media\"> <wb-icon size=80px>recent_actors</wb-icon> </div> </md-card-title-media> </md-card-title> </md-card> <md-card style=\"width: 40%\" class=value-card> <md-card-title> <md-card-title-text style=\"margin-left: 16px\"> <span class=md-headline>{{metrics['supertms.test.count'] || 0}}</span> <span class=md-subhead translate>Number of Tests</span>  </md-card-title-text> <md-card-title-media> <div class=\"md-media-sm card-media\"> <wb-icon size=80px>assignment_turned_in</wb-icon> </div> </md-card-title-media> </md-card-title> </md-card>                  <md-card style=\"width: 40%\" class=value-card> <md-card-title> <md-card-title-text style=\"margin-left: 16px\"> <span class=md-headline>{{metrics['supertms.test_run.count'] || 0}}</span> <span class=md-subhead translate>Number of Test Runs</span>  </md-card-title-text> <md-card-title-media> <div class=\"md-media-sm card-media\"> <wb-icon size=80px>receipt</wb-icon> </div> </md-card-title-media> </md-card-title> </md-card> </div> </mb-titled-block> <mb-pagination-bar mb-icon=history mb-title=\"{{'Running Jobs' | translate}}\" mb-model=paginatorParameter mb-reload=reload() mb-properties=ctrl.properties> </mb-pagination-bar>  <div mb-preloading=ctrl.loadingPipelines layout=column flex> <md-list> <md-list-item ng-repeat=\"object in ctrl.items track by object.id\" class=md-3-line> <wb-icon ng-if=\"object.status == 'new'\">star_border</wb-icon> <wb-icon ng-if=\"object.status == 'ready'\">access_time</wb-icon> <wb-icon ng-if=\"object.status == 'in-progress'\">play_circle_outline</wb-icon> <wb-icon ng-if=\"object.status == 'stopped'\">pause_circle_outline</wb-icon> <wb-icon ng-if=\"object.status == 'error'\">error_outline</wb-icon> <wb-icon ng-if=\"object.status == 'done'\">done</wb-icon> <div class=md-list-item-text layout=column> <h3>{{object.id}} - {{object.title}}</h3> <p><span translate>Status</span>: <span translate>{{object.status}}</span></p> <p><span translate>Tenant</span>: {{tenants[object.tenant].title}}</p> </div> <md-divider md-inset></md-divider> </md-list-item> </md-list> <div layout=column layout-align=\"center center\" ng-show=\"!ctrl.loadingPipelines && ctrl.items && ctrl.items.length === 0\"> <h2 translate>Empty job list</h2> </div> </div> </md-content>"
  );

}]);

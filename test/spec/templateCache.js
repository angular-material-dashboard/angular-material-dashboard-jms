angular.module('ngMaterialDashboardJms').run(['$templateCache', function($templateCache) {
  'use strict';

  $templateCache.put('views/amd-jms-pipeline-new.html',
    ""
  );


  $templateCache.put('views/amd-jms-pipeline.html',
    ""
  );


  $templateCache.put('views/amd-jms-pipelines.html',
    "<md-content flex layout-padding mb-infinate-scroll=nextPage()> <mb-titled-block mb-icon=report mb-title=\"{{'Statistics' | translate}}\" mb-progress=ctrl.loadingStatistics> <div layout=row layout-align=\"center center\" layout-wrap layout-padding> <md-card style=\"width: 40%\" class=value-card> <md-card-title> <md-card-title-text style=\"margin-left: 16px\"> <span class=md-headline>{{metrics['supertenant.tenant.count'] || 0}}</span> <span class=md-subhead translate>Number of Organizations</span>  </md-card-title-text> <md-card-title-media> <div class=\"md-media-sm card-media\"> <wb-icon size=80px>business</wb-icon> </div> </md-card-title-media> </md-card-title> </md-card> <md-card style=\"width: 40%\" class=value-card> <md-card-title> <md-card-title-text style=\"margin-left: 16px\"> <span class=md-headline>{{metrics['supertenant.member.count'] || 0}}</span> <span class=md-subhead translate>Number of Users</span>  </md-card-title-text> <md-card-title-media> <div class=\"md-media-sm card-media\"> <wb-icon size=80px>recent_actors</wb-icon> </div> </md-card-title-media> </md-card-title> </md-card> <md-card style=\"width: 40%\" class=value-card> <md-card-title> <md-card-title-text style=\"margin-left: 16px\"> <span class=md-headline>{{metrics['supertms.test.count'] || 0}}</span> <span class=md-subhead translate>Number of Tests</span>  </md-card-title-text> <md-card-title-media> <div class=\"md-media-sm card-media\"> <wb-icon size=80px>assignment_turned_in</wb-icon> </div> </md-card-title-media> </md-card-title> </md-card>                  <md-card style=\"width: 40%\" class=value-card> <md-card-title> <md-card-title-text style=\"margin-left: 16px\"> <span class=md-headline>{{metrics['supertms.test_run.count'] || 0}}</span> <span class=md-subhead translate>Number of Test Runs</span>  </md-card-title-text> <md-card-title-media> <div class=\"md-media-sm card-media\"> <wb-icon size=80px>receipt</wb-icon> </div> </md-card-title-media> </md-card-title> </md-card> </div> </mb-titled-block> <mb-pagination-bar mb-icon=history mb-title=\"{{'Running Jobs' | translate}}\" mb-model=paginatorParameter mb-reload=reload() mb-properties=ctrl.properties> </mb-pagination-bar>  <div mb-preloading=ctrl.loadingPipelines layout=column flex> <md-list> <md-list-item ng-repeat=\"object in ctrl.items track by object.id\" class=md-3-line> <wb-icon ng-if=\"object.status == 'new'\">star_border</wb-icon> <wb-icon ng-if=\"object.status == 'ready'\">access_time</wb-icon> <wb-icon ng-if=\"object.status == 'in-progress'\">play_circle_outline</wb-icon> <wb-icon ng-if=\"object.status == 'stopped'\">pause_circle_outline</wb-icon> <wb-icon ng-if=\"object.status == 'error'\">error_outline</wb-icon> <wb-icon ng-if=\"object.status == 'done'\">done</wb-icon> <div class=md-list-item-text layout=column> <h3>{{object.id}} - {{object.title}}</h3> <p><span translate>Status</span>: <span translate>{{object.status}}</span></p> <p><span translate>Tenant</span>: {{tenants[object.tenant].title}}</p> </div> <md-divider md-inset></md-divider> </md-list-item> </md-list> <div layout=column layout-align=\"center center\" ng-show=\"!ctrl.loadingPipelines && ctrl.items && ctrl.items.length === 0\"> <h2 translate>Empty job list</h2> </div> </div> </md-content>"
  );

}]);
